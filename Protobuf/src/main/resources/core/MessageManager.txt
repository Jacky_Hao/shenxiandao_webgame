package org.yunai.protobuf.base;

import org.yunai.yfserver.message.AbstractDecoder;
import org.yunai.yfserver.message.AbstractEncoder;
import java.util.HashMap;
import java.util.Map;

/**
 * 消息管理器
 * User: yunai
 * Date: 13-3-14
 * Time: 下午3:24
 */
public class MessageManager {

    private static Map<Short, AbstractDecoder> decoderMap;
    private static Map<Short, AbstractEncoder> encoderMap;

    static {
        decoderMap = new HashMap<Short, AbstractDecoder>();
        encoderMap = new HashMap<Short, AbstractEncoder>();

        /** init **/
    }

    public static AbstractDecoder getDecoder(Short msgId) {
        return decoderMap.get(msgId);
    }

    public static AbstractEncoder getEncoder(Short msgId) {
        return encoderMap.get(msgId);
    }
}