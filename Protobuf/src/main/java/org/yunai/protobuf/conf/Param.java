package org.yunai.protobuf.conf;

import com.alibaba.fastjson.JSON;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

/**
 * 消息参数
 * User: yunai
 * Date: 13-3-13
 * Time: 上午9:40
 */
public class Param {

    public static final String CONFIG_ROOT = "Param";

    private static final String ATTRIBUTE_NAME = "name";
    private static final String ATTRIBUTE_TYPE = "type";
    private static final String ATTRIBUTE_DESC = "desc";

    /**
     * 参数名
     */
    private String name;
    /**
     * 类型<br />
     */
    private String type;
    /**
     * 描述
     */
    private String desc;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Param(String name, String type, String desc) {
        this.name = name;
        this.type = type;
        this.desc = desc;
    }

    public static Param parseParamNode(CodeClass codeClazz, Node node) {
        NamedNodeMap attributes = node.getAttributes();
        try {
            return new Param(
                    attributes.getNamedItem(ATTRIBUTE_NAME).getNodeValue(),
                    attributes.getNamedItem(ATTRIBUTE_TYPE).getNodeValue(),
                    attributes.getNamedItem(ATTRIBUTE_DESC).getNodeValue()

            );
        } catch (Exception e) {
System.err.println("消息信息：" + JSON.toJSONString(codeClazz));
            throw new RuntimeException(e);
        }
    }
}
