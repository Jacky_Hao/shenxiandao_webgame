package org.yunai.protobuf.conf;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * 结构体配置
 * User: yunai
 * Date: 13-3-13
 * Time: 上午10:47
 */
public class StructConfig {

    private static final String CONFIG_ROOT = "StructConfig";

    private static final String STRUCT_ROOT = "Struct";
    private static final String STRUCT_ATTRIBUTE_NAME = "name";
    private static final String STRUCT_ATTRIBUTE_DESC = "desc";

    public static List<Struct> structs;

    public static class Struct implements CodeClass {

        /**
         * 消息名
         */
        private String name;
        /**
         * 描述
         */
        private String desc;
        /**
         * 参数列表
         */
        private List<Param> params;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

        public List<Param> getParams() {
            return params;
        }

        public void setParams(List<Param> params) {
            this.params = params;
        }

        @Override
        public List<Param> obtainParams() {
            return getParams();
        }

        @Override
        public String obtainClassName() {
            return getName();
        }
    }

    public static void load() {
        InputStream is = null;
        try {
            // 创建InputStream
            URL url = Thread.currentThread().getContextClassLoader().getResource(Config.filePath);
            if (url == null) {
                throw new FileNotFoundException(url + "文件不存在！");
            }
            is = new FileInputStream(url.getFile());

            // XML解析
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document doc = builder.parse(is);

            // 解析StructConfig节点
            NodeList structConfigNodes = doc.getElementsByTagName(CONFIG_ROOT);
            if (structConfigNodes.getLength() > 1) {
                throw new SAXException("<" + CONFIG_ROOT + ">只允许有一个！");
            }
            Node structConfigNode = structConfigNodes.item(0);

            // 解析Struct节点
            structs = new ArrayList<Struct>();
            NodeList structNodes = structConfigNode.getChildNodes();
            for (int i = 0, len = structNodes.getLength(); i < len; i++) {
                Node structNode = structNodes.item(i);
                if (!STRUCT_ROOT.equals(structNode.getNodeName())) {
                    continue;
                }
                parseStructNode(structNode);
            }

        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static void parseStructNode(Node node) {
        Struct struct = new Struct();
        NamedNodeMap attributes  = node.getAttributes();
        struct.setName(attributes.getNamedItem(STRUCT_ATTRIBUTE_NAME).getNodeValue());
        struct.setDesc(attributes.getNamedItem(STRUCT_ATTRIBUTE_DESC).getNodeValue());
        structs.add(struct);

        // 解析Param节点
        NodeList paramNodes = node.getChildNodes();
        List<Param> params = new ArrayList<Param>();
        struct.setParams(params);
        for (int i = 0, len = paramNodes.getLength(); i < len; i++) {
            Node paramNode = paramNodes.item(i);
            if (!Param.CONFIG_ROOT.equals(paramNode.getNodeName())) {
                continue;
            }
            Param param = Param.parseParamNode(struct, paramNode);
            params.add(param);
        }
    }
}
