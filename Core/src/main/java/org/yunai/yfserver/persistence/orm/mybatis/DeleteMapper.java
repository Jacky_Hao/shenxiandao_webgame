package org.yunai.yfserver.persistence.orm.mybatis;

import org.yunai.yfserver.persistence.orm.Entity;

/**
 * 删除操作Mapper接口。包含delete操作<br />
 * @see org.yunai.yfserver.persistence.updater.PersistenceObjectUpdater
 * User: yunai
 * Date: 13-4-9
 * Time: 下午8:59
 */
public interface DeleteMapper<E extends Entity> {

    /**
     * 删除数据
     * @param entity 数据
     */
    void delete(E entity);
}
