package org.yunai.yfserver.annotation;

import java.lang.annotation.*;

/**
 * 用于标识属性定义时的类型的注释
 * User: yunai
 * Date: 13-5-2
 * Time: 下午8:22
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Type {

    /**
     * @return 类
     */
    Class<?> value();
}
