import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created with IntelliJ IDEA.
 * User: yunai
 * Date: 13-3-16
 * Time: 下午10:41
 */
public class ScheduleExecutorServiceTest {

    public static void main(String[] args) {
        Runnable task = new Runnable() {
            public void run() {
                System.out.println("date:" + new Date());
//                throw new RuntimeException();
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        };

        ScheduledExecutorService scheduExec = Executors.newScheduledThreadPool(1);
System.out.println("调度前xx：" + new Date());
        scheduExec.scheduleWithFixedDelay(task, 0, 3000, TimeUnit.MILLISECONDS);
    }
}
