package _java.uti;

import java.util.BitSet;

/**
 * BitSet测试类
 * User: yunai
 * Date: 13-5-16
 * Time: 上午10:17
 */
public class BitSetTest {

    public static void main(String[] args) {
        BitSet bitSet = new BitSet(3);
        System.out.println(bitSet.isEmpty());
        System.out.println(bitSet.length());
        bitSet.flip(0, 3);
        System.out.println(bitSet.get(0));
    }
}
