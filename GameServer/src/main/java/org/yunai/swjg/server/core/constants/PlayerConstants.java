package org.yunai.swjg.server.core.constants;

/**
 * 玩家属性默认配置
 * User: yunai
 * Date: 13-5-3
 * Time: 下午7:48
 */
public class PlayerConstants {

    /**
     * 默认场景编号
     */
    public static final int DEFAULT_SCENE_ID = 1;
    /**
     * 默认场景X
     */
    public static final short DEFAULT_SCENE_X = 1;
    /**
     * 默认场景Y
     */
    public static final short DEFAULT_SCENE_Y = 2;
    /**
     * 最大等级
     */
    public static final short LEVEL_MAX = 10;

    /**
     * 货币 - 游戏币最大值
     */
    public static final Integer CURRENCY_COIN_MAX = 100000000;
    /**
     * 货币 - 元宝最大值
     */
    public static final Integer CURRENCY_GOLD_MAX = 100000000;
}
