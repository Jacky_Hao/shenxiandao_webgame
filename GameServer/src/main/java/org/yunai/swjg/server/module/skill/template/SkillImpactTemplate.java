package org.yunai.swjg.server.module.skill.template;

import org.jumpmind.symmetric.csv.CsvReader;
import org.yunai.swjg.server.module.skill.SkillDef;
import org.yunai.yfserver.util.CsvUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 技能影响模版<br />
 * 每个技能影响会带多个效果
 * User: yunai
 * Date: 13-6-11
 * Time: 下午12:34
 */
public class SkillImpactTemplate {

    private static final String KEY_EFFECT_BASE = "effect";
    private static final int EFFECT_MAX = 3;

    private int sn;
    private String name;
    private List<SkillDef.Effect> effects;
    private List<SkillDef.Effect> buffDebuffEffects;
    private List<SkillDef.Effect> hotDotEffects;
    private List<SkillDef.Effect> functionEffects;

    public int getSn() {
        return sn;
    }

    public String getName() {
        return name;
    }

    public List<SkillDef.Effect> getEffects() {
        return effects;
    }

    public List<SkillDef.Effect> getBuffDebuffEffects() {
        return buffDebuffEffects;
    }

    public List<SkillDef.Effect> getHotDotEffects() {
        return hotDotEffects;
    }

    public List<SkillDef.Effect> getFunctionEffects() {
        return functionEffects;
    }

    // ==================== 非set/get方法 ====================
    private static Map<Integer, SkillImpactTemplate> templates;

    public static SkillImpactTemplate getItemTemplate(Integer sn) {
        return templates.get(sn);
    }

    public static void load() {
        CsvReader reader = null;
        // 道具公用模板
        templates = new HashMap<>();
        try {
            reader = CsvUtil.createReader("csv/skill/skill_affect_template.csv");
            reader.readHeaders();
            while (reader.readRecord()) {
                SkillImpactTemplate template = genSkillImpactTemplate(reader);
                templates.put(template.getSn(), template);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
    }

    private static SkillImpactTemplate genSkillImpactTemplate(CsvReader reader) throws IOException {
        SkillImpactTemplate template = new SkillImpactTemplate();
        template.sn = CsvUtil.getInt(reader, "sn", 0);
        template.name = CsvUtil.getString(reader, "name", "");
        template.effects = new ArrayList<>(EFFECT_MAX);
        for (int i = 1; i <= EFFECT_MAX; i++) {
            if (CsvUtil.getString(reader, KEY_EFFECT_BASE + i, "").equals("")) {
                break;
            }
            SkillDef.Effect effect = SkillDef.Effect.valueOf(CsvUtil.getInt(reader, KEY_EFFECT_BASE + i, 0));
            if (effect == null) {
                throw new IllegalArgumentException("参数错误: " + template.getSn());
            }
            template.effects.add(effect);
            template.buffDebuffEffects = new ArrayList<>(EFFECT_MAX);
            template.hotDotEffects = new ArrayList<>(EFFECT_MAX);
            template.functionEffects = new ArrayList<>(EFFECT_MAX);
            switch (effect.getType()) {
                case BUFF_DEBUFF:
                    template.buffDebuffEffects.add(effect);
                    break;
                case HOT_DOT:
                    template.hotDotEffects.add(effect);
                    break;
                case FUNCTION:
                    template.functionEffects.add(effect);
                    break;
            }
        }
        return template;
    }
}
