package org.yunai.swjg.server.module.battle.operation;

import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.monster.vo.VisibleMonster;

/**
 * 玩家与副本怪物战斗操作
 * User: yunai
 * Date: 13-5-17
 * Time: 下午11:02
 */
public class PVMMonsterBattleOperation extends PVMBattleOperation {

    public PVMMonsterBattleOperation(Online online, VisibleMonster monster) {
        super(online, monster, true, true);
    }

    @Override
    protected void pvmEndImpl(boolean attWin) {

//        monster.endBattle(attWin);
//
//        // TODO 输了战斗进行复活
//        if (!attWin) {
//
//        }
        monster.getScene().afterBattleMonster(online, monster, attWin);
    }

}
