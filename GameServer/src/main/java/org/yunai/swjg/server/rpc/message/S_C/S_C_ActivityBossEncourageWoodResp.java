package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21619】: 使用元宝鼓舞响应
 */
public class S_C_ActivityBossEncourageWoodResp extends GameMessage {
    public static final short CODE = 21619;

    /**
     * 是否鼓舞成功
     */
    private Byte success;

    public S_C_ActivityBossEncourageWoodResp() {
    }

    public S_C_ActivityBossEncourageWoodResp(Byte success) {
        this.success = success;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Byte getSuccess() {
		return success;
	}

	public void setSuccess(Byte success) {
		this.success = success;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_ActivityBossEncourageWoodResp struct = new S_C_ActivityBossEncourageWoodResp();
            struct.setSuccess(byteArray.getByte());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_ActivityBossEncourageWoodResp struct = (S_C_ActivityBossEncourageWoodResp) message;
            ByteArray byteArray = ByteArray.createNull(1);
            byteArray.create();
            byteArray.putByte(struct.getSuccess());
            return byteArray;
        }
    }
}