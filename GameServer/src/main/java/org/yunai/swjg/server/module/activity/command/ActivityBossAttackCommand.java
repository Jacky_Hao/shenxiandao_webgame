package org.yunai.swjg.server.module.activity.command;

import org.springframework.stereotype.Controller;
import org.yunai.swjg.server.core.constants.SysMessageConstants;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.activity.ActivityService;
import org.yunai.swjg.server.module.activity.BossActivity;
import org.yunai.swjg.server.rpc.message.C_S.C_S_ActivityBossAttackReq;
import org.yunai.swjg.server.rpc.message.S_C.S_C_SysMessageReq;

import javax.annotation.Resource;

/**
 * BOSS活动攻击怪物命令
 * User: yunai
 * Date: 13-5-23
 * Time: 下午7:01
 */
@Controller
public class ActivityBossAttackCommand extends GameMessageCommand<C_S_ActivityBossAttackReq> {

    @Resource
    private ActivityService activityService;

    @Override
    protected void execute(Online online, C_S_ActivityBossAttackReq msg) {
        BossActivity bossActivity = activityService.getBossActivity();
        if (bossActivity == null) {
            online.write(new S_C_SysMessageReq(SysMessageConstants.MONSTER_NOT_FOUND));
            return;
        }
        bossActivity.playerBeginAttack(online, msg.getId());
    }
}
