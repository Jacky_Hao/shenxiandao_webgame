package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21618】: 使用元宝鼓舞请求
 */
public class C_S_ActivityBossEncourageWoodReq extends GameMessage {
    public static final short CODE = 21618;

    public C_S_ActivityBossEncourageWoodReq() {
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            return new C_S_ActivityBossEncourageWoodReq();
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_ActivityBossEncourageWoodReq struct = (C_S_ActivityBossEncourageWoodReq) message;
            ByteArray byteArray = ByteArray.createNull(0);
            byteArray.create();
            return byteArray;
        }
    }
}