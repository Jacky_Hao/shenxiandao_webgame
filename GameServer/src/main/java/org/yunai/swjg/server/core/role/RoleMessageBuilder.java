package org.yunai.swjg.server.core.role;

import org.yunai.swjg.server.rpc.message.S_C.S_C_RoleIntInfoReq;
import org.yunai.swjg.server.rpc.message.S_C.S_C_RoleStringInfoReq;
import org.yunai.swjg.server.rpc.struct.StKeyValuePairInt;
import org.yunai.swjg.server.rpc.struct.StKeyValuePairString;
import org.yunai.yfserver.object.KeyValuePair;

import java.util.ArrayList;
import java.util.List;

/**
 * 属性消息Builder
 * User: yunai
 * Date: 13-5-6
 * Time: 上午5:52
 */
public class RoleMessageBuilder {

    public static S_C_RoleIntInfoReq build(Role role, List<KeyValuePair<Integer, Integer>> props) {
        List<StKeyValuePairInt> pairs = new ArrayList<>(props.size());
        for (KeyValuePair<Integer, Integer> prop : props) {
            pairs.add(new StKeyValuePairInt(prop.getKey(), prop.getValue()));
        }
        return new S_C_RoleIntInfoReq(role.getId(), pairs);
    }

    public static S_C_RoleStringInfoReq build2(Role role, List<KeyValuePair<Integer, Object>> props) {
        List<StKeyValuePairString> pairs = new ArrayList<>(props.size());
        for (KeyValuePair<Integer, Object> prop : props) {
            try {
                pairs.add(new StKeyValuePairString(prop.getKey(), prop.getValue().toString()));
            } catch (Exception e) {
                System.err.println(prop.toString());
            }
        }
        return new S_C_RoleStringInfoReq(role.getId(), pairs);
    }
}
