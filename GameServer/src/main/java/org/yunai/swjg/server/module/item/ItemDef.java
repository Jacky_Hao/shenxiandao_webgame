package org.yunai.swjg.server.module.item;

import org.yunai.yfserver.enums.IndexedEnum;

import java.util.List;

/**
 * 道具相关类型、常量定义
 * User: yunai
 * Date: 13-4-6
 * Time: 下午2:12
 */
public interface ItemDef {

    /**
     * 品质
     */
    public static enum Quality implements IndexedEnum {
        WHILE((short) 1),
        GREEN((short) 2),
        BLUE((short) 3),
        PURPLE((short) 4);

        private final short quality;

        private Quality(short quality) {
            this.quality = quality;
        }

        @Override
        public int getIndex() {
            return quality;
        }

        private static final List<Quality> VALUES = IndexedEnum.Util.toIndexes(values());

        public static Quality valueOf(short index) {
            return IndexedEnum.Util.valueOf(VALUES, index);
        }
    }

    /**
     * 道具身份类型<br />
     * 一级属性
     */
    public static enum IdentityType implements IndexedEnum {
        /**
         * 装备
         */
        EQUIPMENT(10),
        /**
         * 消耗品
         */
        CONSUMABLE(11),
        /**
         * 卷轴
         */
        REEL(12),
        /**
         * 材料
         */
        MATERIAL(13);

        private final int index;

        private IdentityType(int index) {
            this.index = index;
        }

        private static final List<IdentityType> VALUES = Util.toIndexes(values());

        public static IdentityType valueOf(Integer index) {
            return Util.valueOf(VALUES, index);
        }

        @Override
        public int getIndex() {
            return index;
        }
    }

    /**
     * 道具类型<br />
     * 二级属性
     */
    public static enum Type implements IndexedEnum {

        // ==================== 装备 ====================
        /**
         * 武器
         */
        WEAPON(101, IdentityType.EQUIPMENT),
        /**
         * 衣服
         */
        CLOTHES(102, IdentityType.EQUIPMENT),
        /**
         * 头盔
         */
        CAP(103, IdentityType.EQUIPMENT),
        /**
         * 鞋子
         */
        SHOES(104, IdentityType.EQUIPMENT),
        /**
         * 护符
         */
        AMULET(105, IdentityType.EQUIPMENT),
        /**
         * 饰品
         */
        ACCESSORIES(106, IdentityType.EQUIPMENT),

        // ==================== 消耗品 ====================
        /**
         * 丹药
         */
        MEDICINE_CONSUMABLE(111, IdentityType.CONSUMABLE),

        // ==================== 卷轴 ====================
        /**
         * 装备合成卷轴
         */
        EQUIP_REEL(121, IdentityType.REEL),
        /**
         * 丹药合成卷轴
         */
        MEDICINE_REEL(122, IdentityType.REEL),

        // ==================== 材料 ====================
        /**
         * 任务材料
         */
        QUEST_MATERIAL(131, IdentityType.MATERIAL),
        /**
         * 合成材料
         */
        SYNTHETIC_MATERIAL(132, IdentityType.MATERIAL),;

        public final int index;
        /**
         * 所属大类
         */
        public final IdentityType identityType;

        private Type(int index, IdentityType identityType) {
            this.index = index;
            this.identityType = identityType;
        }

        @Override
        public int getIndex() {
            return index;
        }

        public IdentityType getIdentityType() {
            return identityType;
        }

        /**
         * 是否是装备类型
         *
         * @return
         */
        public boolean isEquipment() {
            return IdentityType.EQUIPMENT == identityType;
        }

        private static final List<Type> values = IndexedEnum.Util.toIndexes(Type.values());

        public static Type valueOf(int index) {
            return IndexedEnum.Util.valueOf(values, index);
        }
    }
}