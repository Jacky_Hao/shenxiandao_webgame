package org.yunai.swjg.server.core.config;

/**
 * 配置接口,用于提供系统配置内容
 * User: yunai
 * Date: 13-4-23
 * Time: 下午5:31
 */
public interface Config {

    /**
     * 取得系统配置的版本号,该版本号不代表程序的真实版本号,只是声明的版本号
     */
    String getVersion();

    /**
     * 校验配置参数是否符合有效
     *
     * @throws IllegalArgumentException 如果有参数配置错误,会抛出此异常
     */
    void validate();

    /**
     * @return 是否是调试模式
     */
    public boolean isDebug();
}
