package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21005】: 完成任务请求
 */
public class C_S_FinishQuestReq extends GameMessage {
    public static final short CODE = 21005;

    /**
     * 任务编号
     */
    private Integer questId;

    public C_S_FinishQuestReq() {
    }

    public C_S_FinishQuestReq(Integer questId) {
        this.questId = questId;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Integer getQuestId() {
		return questId;
	}

	public void setQuestId(Integer questId) {
		this.questId = questId;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            C_S_FinishQuestReq struct = new C_S_FinishQuestReq();
            struct.setQuestId(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_FinishQuestReq struct = (C_S_FinishQuestReq) message;
            ByteArray byteArray = ByteArray.createNull(4);
            byteArray.create();
            byteArray.putInt(struct.getQuestId());
            return byteArray;
        }
    }
}