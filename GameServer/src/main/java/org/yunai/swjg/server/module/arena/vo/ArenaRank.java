package org.yunai.swjg.server.module.arena.vo;

import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.entity.ArenaRankEntity;
import org.yunai.swjg.server.module.player.SexEnum;
import org.yunai.swjg.server.module.player.VocationEnum;
import org.yunai.swjg.server.module.player.vo.Player;
import org.yunai.yfserver.persistence.PersistenceObject;
import org.yunai.yfserver.spring.BeanManager;
import org.yunai.yfserver.time.TimeService;

/**
 * 降机场排行数据
 * User: yunai
 * Date: 13-5-29
 * Time: 上午12:18
 */
public class ArenaRank implements PersistenceObject<Integer, ArenaRankEntity> {

    private static TimeService timeService;
    static {
        timeService = BeanManager.getBean(TimeService.class);
    }

    /**
     * 玩家编号
     */
    private int id;
    /**
     * 最后充值时间
     */
    private long lastResetTime;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 等级
     */
    private short level;
    /**
     * 职业
     */
    private VocationEnum vocation;
    /**
     * 性别
     */
    private SexEnum sex;
    /**
     * 剩余挑战次数
     */
    private int challenge;
    /**
     * 挑战购买次数
     */
    private int challengeBuy;
    /**
     * 再次可可以挑战时间，单位（毫秒）。<br />
     * 0代表无CD
     */
    private long challengeCD;
    /**
     * 连胜次数
     */
    private int winStreak;
    /**
     * 排行
     */
    private int rank;
    /**
     * 上上次排行
     */
    private int lastLastRank;
    /**
     * 上次排行
     */
    private int lastRank;
    /**
     * 角色队伍战力
     */
    private int power;
    /**
     * 是否在数据库中
     */
    private boolean inDB;

    private ArenaRank() {
    }

    @Override
    public void fromEntity(ArenaRankEntity entity) {
        this.id = entity.getId();
        this.lastResetTime = entity.getLastResetTime();
        this.nickname = entity.getNickname();
        this.level = entity.getLevel();
        this.vocation = VocationEnum.valueOf(entity.getVocation());
        this.sex = SexEnum.valueOf(entity.getSex());
        this.challenge = entity.getChallenge();
        this.challengeCD = entity.getChallengeCD();
        this.winStreak = entity.getWinStreak();
        this.rank = entity.getRank();
        this.lastLastRank = entity.getLastLastRank();
        this.lastRank = entity.getLastRank();
        this.power = entity.getPower();
    }

    @Override
    public ArenaRankEntity toEntity() {
        ArenaRankEntity entity = new ArenaRankEntity();
        entity.setId(id);
        entity.setLastResetTime(lastResetTime);
        entity.setNickname(nickname);
        entity.setLevel(level);
        entity.setVocation(vocation.getCode());
        entity.setSex(sex.getCode());
        entity.setChallenge(challenge);
        entity.setChallengeCD(challengeCD);
        entity.setWinStreak(winStreak);
        entity.setRank(rank);
        entity.setLastLastRank(lastLastRank);
        entity.setLastRank(lastRank);
        entity.setPower(power);
        return entity;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public boolean isInDB() {
        return inDB;
    }

    @Override
    public void setInDB(boolean inDB) {
        this.inDB = inDB;
    }

    @Override
    public Integer getUnitId() { // TODO 之后考虑
        return id;
    }

    public long getLastResetTime() {
        return lastResetTime;
    }

    public void setLastResetTime(long lastResetTime) {
        this.lastResetTime = lastResetTime;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public short getLevel() {
        return level;
    }

    public void setLevel(short level) {
        this.level = level;
    }

    public VocationEnum getVocation() {
        return vocation;
    }

    public void setVocation(VocationEnum vocation) {
        this.vocation = vocation;
    }

    public SexEnum getSex() {
        return sex;
    }

    public void setSex(SexEnum sex) {
        this.sex = sex;
    }

    public int getChallenge() {
        return challenge;
    }

    public void setChallenge(int challenge) {
        this.challenge = challenge;
    }

    public int getChallengeBuy() {
        return challengeBuy;
    }

    public void setChallengeBuy(int challengeBuy) {
        this.challengeBuy = challengeBuy;
    }

    public long getChallengeCD() {
        return challengeCD;
    }

    public void setChallengeCD(long challengeCD) {
        this.challengeCD = challengeCD;
    }

    public int getWinStreak() {
        return winStreak;
    }

    public void setWinStreak(int winStreak) {
        this.winStreak = winStreak;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getLastLastRank() {
        return lastLastRank;
    }

    public void setLastLastRank(int lastLastRank) {
        this.lastLastRank = lastLastRank;
    }

    public int getLastRank() {
        return lastRank;
    }

    public void setLastRank(int lastRank) {
        this.lastRank = lastRank;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    // ==================== 业务方法BEGIN ====================
    public static ArenaRank build(ArenaRankEntity entity) {
        ArenaRank arenaRank = new ArenaRank();
        arenaRank.inDB = true;
        return arenaRank;
    }

    public static ArenaRank save(Online online, int rank) {
        ArenaRank arenaRank = new ArenaRank();
        arenaRank.inDB = false;
        Player player = online.getPlayer();
        arenaRank.id = player.getId();
        arenaRank.lastResetTime = timeService.now();
        arenaRank.level = player.getLevel();
        arenaRank.vocation = player.getVocation();
        arenaRank.sex = player.getSex();
        arenaRank.challenge = 0;
        arenaRank.challengeBuy = 0;
        arenaRank.winStreak = 0;
        arenaRank.rank = rank;
        arenaRank.lastLastRank = rank;
        arenaRank.power = -1; // TODO 等出队伍后在弄
        // TODO 没保存呢
        return arenaRank;
    }
}
