package org.yunai.swjg.server.module.player;

import org.yunai.yfserver.enums.IndexedEnum;

import java.util.List;

/**
 * 性别枚举
 * User: yunai
 * Date: 13-5-3
 * Time: 上午12:05
 */
public enum SexEnum implements IndexedEnum {

    /**
     * 男
     */
    MALE((short) 1),
    /**
     * 女
     */
    FEMALE((short) 2);

    /**
     * 性别编号
     */
    private final short code;

    public static final List<SexEnum> VALUES = Util.toIndexes(values());

    private SexEnum(short code) {
        this.code = code;
    }

    @Override
    public int getIndex() {
        return code;
    }

    /**
     * @return 性别编号
     */
    public short getCode() {
        return code;
    }

    /**
     * @param code 性别编号
     * @return 是否合法
     */
    public static boolean check(short code) {
        return valueOf(code) != null;
    }

    public static SexEnum valueOf(short code) {
        return Util.valueOf(VALUES, code);
    }
}
