package org.yunai.swjg.server.entity;

import org.yunai.yfserver.persistence.orm.Entity;

/**
 * 玩家信息实体
 * User: yunai
 * Date: 13-3-26
 * Time: 下午4:02
 */
public class PlayerEntity implements Entity<Integer> {

    /**
     * DB索引 - uid + serverId的唯一索引
     */
    public static final String DB_INDEX_UID_SERVERID = "idx_uid_serverId";
    /**
     * DB索引 - serverId + nickname的唯一索引
     */
    public static final String DB_INDEX_SERVERID_NICKNAME = "idx_serverId_nickname";

    /**
     * 玩家编号
     */
    private int id;
    // ==================== 角色固定属性 ====================
    /**
     * 帐号编号
     */
    private int uid;
    /**
     * 服务器编号
     */
    private short serverId;
    /**
     * 昵称
     */
    private String nickname;
    /**
     * 职业<br />
     *
     * @see org.yunai.swjg.server.module.player.VocationEnum
     */
    private short vocation;
    /**
     * 性别<br />
     *
     * @see org.yunai.swjg.server.module.player.SexEnum
     */
    private short sex;
    // ==================== 场景相关属性 ====================
    /**
     * 所在场景编号
     */
    private int sceneId;
    /**
     * 所在场景X
     */
    private short sceneX;
    /**
     * 所在场景Y
     */
    private short sceneY;
    /**
     * 上一个场景编号
     */
    private int scenePreId;
    /**
     * 上一个场景坐标X
     */
    private short scenePreX;
    /**
     * 上一个场景坐标Y
     */
    private short scenePreY;
    // ==================== 其他相关属性 ====================
    /**
     * 等级
     */
    private short level;
    /**
     * 经验
     */
    private int exp;
    /**
     * 阅历
     */
    private int coin;
    /**
     * 元宝
     */
    private int gold;
    /**
     * 声望
     */
    private int reputation;
    /**
     * 阅历
     */
    private int soul;
    // ==================== 登录/在线相关属性 ====================
    /**
     * 登录时间
     */
    private Long loginTime;
    /**
     * 最后一次登录时间，单位（毫秒）
     */
    private long lastLoginTime;
    /**
     * 最后一次登出时间，单位（毫秒）
     */
    private long lastLogoutTime;
    /**
     * 最后一次登录IP
     */
    private String lastIp;
    /**
     * 角色创建时间，单位（毫秒）
     */
    private long createRoleTime;
    /**
     * 总在线时长，单位（毫秒）
     */
    private long totalOnlineTime;
    /**
     * 上上一次登录，单位（毫秒）
     */
    private long lastLastLoginTime;
    /**
     * 上上一次IP
     */
    private String lastLastIp;
    // ==================== 培养的基础属性 ====================
    /**
     * 培养的武力
     */
    private int developStrength;
    /**
     * 培养的绝技
     */
    private int developStunt;
    /**
     * 培养的法术
     */
    private int developMagic;
    // ==================== 丹药的基础属性 ====================
    /**
     * 丹药的武力
     */
    private String medicineStrength;
    /**
     * 丹药的绝技
     */
    private String medicineStunt;
    /**
     * 丹药的法术
     */
    private String medicineMagic;
    // ==================== 技能 ====================
    /**
     * 绝技
     */
    private int specialSkill;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public short getServerId() {
        return serverId;
    }

    public void setServerId(short serverId) {
        this.serverId = serverId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public short getLevel() {
        return level;
    }

    public void setLevel(short level) {
        this.level = level;
    }

    public Long getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Long loginTime) {
        this.loginTime = loginTime;
    }

    public short getVocation() {
        return vocation;
    }

    public void setVocation(short vocation) {
        this.vocation = vocation;
    }

    public short getSex() {
        return sex;
    }

    public void setSex(short sex) {
        this.sex = sex;
    }

    public int getExp() {
        return exp;
    }

    public void setExp(int exp) {
        this.exp = exp;
    }

    public int getSceneId() {
        return sceneId;
    }

    public void setSceneId(int sceneId) {
        this.sceneId = sceneId;
    }

    public short getSceneX() {
        return sceneX;
    }

    public void setSceneX(short sceneX) {
        this.sceneX = sceneX;
    }

    public short getSceneY() {
        return sceneY;
    }

    public void setSceneY(short sceneY) {
        this.sceneY = sceneY;
    }

    public int getScenePreId() {
        return scenePreId;
    }

    public void setScenePreId(int scenePreId) {
        this.scenePreId = scenePreId;
    }

    public short getScenePreX() {
        return scenePreX;
    }

    public void setScenePreX(short scenePreX) {
        this.scenePreX = scenePreX;
    }

    public short getScenePreY() {
        return scenePreY;
    }

    public void setScenePreY(short scenePreY) {
        this.scenePreY = scenePreY;
    }

    public int getCoin() {
        return coin;
    }

    public void setCoin(int coin) {
        this.coin = coin;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public long getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(long lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public long getLastLogoutTime() {
        return lastLogoutTime;
    }

    public void setLastLogoutTime(long lastLogoutTime) {
        this.lastLogoutTime = lastLogoutTime;
    }

    public String getLastIp() {
        return lastIp;
    }

    public void setLastIp(String lastIp) {
        this.lastIp = lastIp;
    }

    public long getCreateRoleTime() {
        return createRoleTime;
    }

    public void setCreateRoleTime(long createRoleTime) {
        this.createRoleTime = createRoleTime;
    }

    public long getTotalOnlineTime() {
        return totalOnlineTime;
    }

    public void setTotalOnlineTime(long totalOnlineTime) {
        this.totalOnlineTime = totalOnlineTime;
    }

    public long getLastLastLoginTime() {
        return lastLastLoginTime;
    }

    public void setLastLastLoginTime(long lastLastLoginTime) {
        this.lastLastLoginTime = lastLastLoginTime;
    }

    public String getLastLastIp() {
        return lastLastIp;
    }

    public void setLastLastIp(String lastLastIp) {
        this.lastLastIp = lastLastIp;
    }

    public int getDevelopStrength() {
        return developStrength;
    }

    public void setDevelopStrength(int developStrength) {
        this.developStrength = developStrength;
    }

    public int getDevelopStunt() {
        return developStunt;
    }

    public void setDevelopStunt(int developStunt) {
        this.developStunt = developStunt;
    }

    public int getDevelopMagic() {
        return developMagic;
    }

    public void setDevelopMagic(int developMagic) {
        this.developMagic = developMagic;
    }

    public int getReputation() {
        return reputation;
    }

    public void setReputation(int reputation) {
        this.reputation = reputation;
    }

    public int getSoul() {
        return soul;
    }

    public void setSoul(int soul) {
        this.soul = soul;
    }

    public String getMedicineStrength() {
        return medicineStrength;
    }

    public void setMedicineStrength(String medicineStrength) {
        this.medicineStrength = medicineStrength;
    }

    public String getMedicineStunt() {
        return medicineStunt;
    }

    public void setMedicineStunt(String medicineStunt) {
        this.medicineStunt = medicineStunt;
    }

    public String getMedicineMagic() {
        return medicineMagic;
    }

    public void setMedicineMagic(String medicineMagic) {
        this.medicineMagic = medicineMagic;
    }

    public int getSpecialSkill() {
        return specialSkill;
    }

    public void setSpecialSkill(int specialSkill) {
        this.specialSkill = specialSkill;
    }
}