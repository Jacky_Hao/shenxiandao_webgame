package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【20605】: 玩家切换场景请求
 */
public class C_S_SceneSwitchReq extends GameMessage {
    public static final short CODE = 20605;

    /**
     * 场景编号
     */
    private Integer sceneId;

    public C_S_SceneSwitchReq() {
    }

    public C_S_SceneSwitchReq(Integer sceneId) {
        this.sceneId = sceneId;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Integer getSceneId() {
		return sceneId;
	}

	public void setSceneId(Integer sceneId) {
		this.sceneId = sceneId;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            C_S_SceneSwitchReq struct = new C_S_SceneSwitchReq();
            struct.setSceneId(byteArray.getInt());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_SceneSwitchReq struct = (C_S_SceneSwitchReq) message;
            ByteArray byteArray = ByteArray.createNull(4);
            byteArray.create();
            byteArray.putInt(struct.getSceneId());
            return byteArray;
        }
    }
}