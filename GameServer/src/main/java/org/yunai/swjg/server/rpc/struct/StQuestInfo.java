package org.yunai.swjg.server.rpc.struct;

import org.yunai.yfserver.message.*;
import java.util.List;
import org.yunai.swjg.server.rpc.struct.StQuestCondition;

/**
 * 任务详细结构体
 */
public class StQuestInfo implements IStruct {
    /**
     * 任务编号
     */
    private Integer id;
    /**
     * 任务状态
     */
    private Byte status;
    /**
     * 任务完成条件数组
     */
    private List<StQuestCondition> finishConditions;

    public StQuestInfo() {
    }

    public StQuestInfo(Integer id, Byte status, List<StQuestCondition> finishConditions) {
        this.id = id;
        this.status = status;
        this.finishConditions = finishConditions;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}
	public List<StQuestCondition> getFinishConditions() {
		return finishConditions;
	}

	public void setFinishConditions(List<StQuestCondition> finishConditions) {
		this.finishConditions = finishConditions;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            StQuestInfo struct = new StQuestInfo();
            struct.setId(byteArray.getInt());
            struct.setStatus(byteArray.getByte());
		struct.setFinishConditions(getMessageList(StQuestCondition.Decoder.getInstance(), byteArray, StQuestCondition.class));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            StQuestInfo struct = (StQuestInfo) message;
            ByteArray byteArray = ByteArray.createNull(5);
            byte[][] finishConditionsBytes = convertMessageList(byteArray, StQuestCondition.Encoder.getInstance(), struct.getFinishConditions());
            byteArray.create();
            byteArray.putInt(struct.getId());
            byteArray.putByte(struct.getStatus());
            putMessageList(byteArray, finishConditionsBytes);
            return byteArray;
        }
    }
}