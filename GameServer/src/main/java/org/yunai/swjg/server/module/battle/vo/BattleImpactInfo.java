package org.yunai.swjg.server.module.battle.vo;

/**
 * 战斗目标影响信息
 * User: yunai
 * Date: 13-6-15
 * Time: 下午10:40
 */
public class BattleImpactInfo {

    /**
     * 技能影响编号
     */
    private int sn;
    /**
     * 目标战斗单元编号
     */
    private int unitIndex;

    public BattleImpactInfo(int sn, int unitIndex) {
        this.sn = sn;
        this.unitIndex = unitIndex;
    }

}
