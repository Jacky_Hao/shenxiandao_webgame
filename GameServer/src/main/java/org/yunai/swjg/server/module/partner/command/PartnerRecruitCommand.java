package org.yunai.swjg.server.module.partner.command;

import org.springframework.stereotype.Controller;
import org.yunai.swjg.server.core.service.GameMessageCommand;
import org.yunai.swjg.server.core.service.Online;
import org.yunai.swjg.server.module.partner.PartnerService;
import org.yunai.swjg.server.rpc.message.C_S.C_S_PartnerRecruitReq;

import javax.annotation.Resource;

/**
 * 雇佣伙伴命令
 * User: yunai
 * Date: 13-5-31
 * Time: 下午2:13
 */
@Controller
public class PartnerRecruitCommand extends GameMessageCommand<C_S_PartnerRecruitReq> {

    @Resource
    private PartnerService partnerService;

    @Override
    protected void execute(Online online, C_S_PartnerRecruitReq msg) {
        partnerService.recruit(online, msg.getTemplateId());
    }
}
