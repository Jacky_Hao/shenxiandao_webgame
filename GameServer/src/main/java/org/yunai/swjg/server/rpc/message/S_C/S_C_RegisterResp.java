package org.yunai.swjg.server.rpc.message.S_C;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21407】: 注册请求
 */
public class S_C_RegisterResp extends GameMessage {
    public static final short CODE = 21407;

    /**
     * 注册结果编号
     */
    private Byte result;

    public S_C_RegisterResp() {
    }

    public S_C_RegisterResp(Byte result) {
        this.result = result;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public Byte getResult() {
		return result;
	}

	public void setResult(Byte result) {
		this.result = result;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            S_C_RegisterResp struct = new S_C_RegisterResp();
            struct.setResult(byteArray.getByte());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            S_C_RegisterResp struct = (S_C_RegisterResp) message;
            ByteArray byteArray = ByteArray.createNull(1);
            byteArray.create();
            byteArray.putByte(struct.getResult());
            return byteArray;
        }
    }
}