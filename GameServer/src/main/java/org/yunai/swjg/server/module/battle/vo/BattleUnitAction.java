package org.yunai.swjg.server.module.battle.vo;

import java.util.ArrayList;
import java.util.List;

/**
 * 战斗单元行为
 * User: yunai
 * Date: 13-6-15
 * Time: 下午10:01
 */
public class BattleUnitAction {

    private int unitIndex;
//    private BattleDef.ActionType actionType;
    private int endMorale;

    private List<BattleHotDotSelfInfo> hotDotSelfInfos;
    private List<BattleBuffSelfInfo> buffSelfInfos;

    private List<BattleTargetInfo> targetInfos;
    private List<BattleImpactInfo> impactInfos;

    public BattleUnitAction(int unitIndex) {
        this.unitIndex = unitIndex;
        this.hotDotSelfInfos = new ArrayList<>(2);
        this.buffSelfInfos = new ArrayList<>(2);
        this.targetInfos = new ArrayList<>(2);
        this.impactInfos = new ArrayList<>(2);
    }

    public boolean isNullAction() {
        return hotDotSelfInfos.isEmpty()
                && buffSelfInfos.isEmpty()
                && targetInfos.isEmpty()
                && impactInfos.isEmpty();
    }
}