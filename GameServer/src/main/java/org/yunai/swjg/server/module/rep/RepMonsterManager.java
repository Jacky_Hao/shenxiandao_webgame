package org.yunai.swjg.server.module.rep;

import org.slf4j.Logger;
import org.yunai.swjg.server.module.monster.template.VisibleMonsterTemplate;
import org.yunai.swjg.server.module.monster.vo.VisibleMonster;
import org.yunai.yfserver.common.LoggerFactory;

import java.util.*;

/**
 * 副本怪物管理
 * User: yunai
 * Date: 13-5-18
 * Time: 下午5:03
 */
public class RepMonsterManager {

    private static final Logger LOGGER_REP = LoggerFactory.getLogger(LoggerFactory.Logger.rep, RepScene.class);

    /**
     * 副本
     */
    private final RepScene repScene;
    /**
     * 怪物集合
     */
    private Map<Integer, VisibleMonster> monsters;

    public RepMonsterManager(RepScene repScene) {
        this.repScene = repScene;
    }

    public void init() {
        List<VisibleMonsterTemplate> visibleMonsterTemplates = VisibleMonsterTemplate.get(repScene.getRepTemplate().getId());
        monsters = new HashMap<>();
        for (VisibleMonsterTemplate template : visibleMonsterTemplates) {
            VisibleMonster monster = new VisibleMonster(template, repScene);
            monsters.put(monster.getId(), monster);
        }
    }

    public List<VisibleMonster> getAliveMonsters() {
        List<VisibleMonster> alives = new ArrayList<>(monsters.size());
        for (VisibleMonster monster : monsters.values()) {
            if (monster.isAlive()) {
                alives.add(monster);
            }
        }
        return alives;
    }

    public VisibleMonster getMonster(Integer monsterId) {
        return monsters.get(monsterId);
    }

    public int aliveMonsterCount() {
        int count = 0;
        for (VisibleMonster monster : monsters.values()) {
            if (monster.isAlive()) {
                count++;
            }
        }
        return count;
    }
}
