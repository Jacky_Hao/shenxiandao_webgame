package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21401】: 登录请求
 */
public class C_S_LoginReq extends GameMessage {
    public static final short CODE = 21401;

    /**
     * 密码
     */
    private String password;
    /**
     * 服务器编号
     */
    private Short serverId;
    /**
     * 用户名
     */
    private String userName;

    public C_S_LoginReq() {
    }

    public C_S_LoginReq(String password, Short serverId, String userName) {
        this.password = password;
        this.serverId = serverId;
        this.userName = userName;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	public Short getServerId() {
		return serverId;
	}

	public void setServerId(Short serverId) {
		this.serverId = serverId;
	}
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            C_S_LoginReq struct = new C_S_LoginReq();
            struct.setPassword(getString(byteArray));
            struct.setServerId(byteArray.getShort());
            struct.setUserName(getString(byteArray));
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_LoginReq struct = (C_S_LoginReq) message;
            ByteArray byteArray = ByteArray.createNull(2);
            byte[] passwordBytes = convertString(byteArray, struct.getPassword());
            byte[] userNameBytes = convertString(byteArray, struct.getUserName());
            byteArray.create();
            putString(byteArray, passwordBytes);
            byteArray.putShort(struct.getServerId());
            putString(byteArray, userNameBytes);
            return byteArray;
        }
    }
}