package org.yunai.swjg.server.module.partner;

import org.yunai.swjg.server.entity.PartnerEntity;
import org.yunai.yfserver.persistence.orm.mybatis.Mapper;
import org.yunai.yfserver.persistence.orm.mybatis.SaveMapper;

import java.util.List;

/**
 * Partner数据访问接口
 * User: yunai
 * Date: 13-5-30
 * Time: 下午11:08
 */
public interface PartnerMapper extends Mapper, SaveMapper<PartnerEntity> {

    /**
     * 获得玩家伙伴列表
     *
     * @param playerId 玩家编号
     * @return 玩家伙伴列表
     */
    List<PartnerEntity> selectList(int playerId);

    /**
     * 插入数据
     *
     * @param entity 数据
     */
    @Override
    void insert(PartnerEntity entity);

    /**
     * 修改数据
     *
     * @param entity 数据
     */
    @Override
    void update(PartnerEntity entity);
}
