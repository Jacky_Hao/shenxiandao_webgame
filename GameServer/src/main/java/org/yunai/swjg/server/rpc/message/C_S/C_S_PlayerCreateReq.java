package org.yunai.swjg.server.rpc.message.C_S;

import org.yunai.yfserver.message.*;
import org.yunai.yfserver.plugin.mina.command.AbstractMinaMessageCommand;
import org.yunai.swjg.server.core.message.GameMessage;
import org.yunai.yfserver.command.MessageDispatcher;
import org.yunai.yfserver.command.Command;

/**
 * 【21201】: 角色创建请求
 */
public class C_S_PlayerCreateReq extends GameMessage {
    public static final short CODE = 21201;

    /**
     * 昵称
     */
    private String nickname;
    /**
     * 性别
     */
    private Short sex;
    /**
     * 职业
     */
    private Short vocation;

    public C_S_PlayerCreateReq() {
    }

    public C_S_PlayerCreateReq(String nickname, Short sex, Short vocation) {
        this.nickname = nickname;
        this.sex = sex;
        this.vocation = vocation;
    }

    @Override
    public short getCode() {
        return CODE;
    }


@SuppressWarnings("unchecked")

@Override
    public void execute() {
        for (Command command : MessageDispatcher.getInstance().getCommands(CODE)) {
            ((AbstractMinaMessageCommand) command).execute(getSession(), this);
        }
    }

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public Short getSex() {
		return sex;
	}

	public void setSex(Short sex) {
		this.sex = sex;
	}
	public Short getVocation() {
		return vocation;
	}

	public void setVocation(Short vocation) {
		this.vocation = vocation;
	}

    public static class Decoder extends AbstractDecoder {
        private static Decoder decoder = new Decoder();

        public static Decoder getInstance() {
            return decoder;
        }

        public IStruct decode(ByteArray byteArray) {
            C_S_PlayerCreateReq struct = new C_S_PlayerCreateReq();
            struct.setNickname(getString(byteArray));
            struct.setSex(byteArray.getShort());
            struct.setVocation(byteArray.getShort());
            return struct;
        }
    }

    public static class Encoder extends AbstractEncoder {
        private static Encoder encoder = new Encoder();

        public static Encoder getInstance() {
            return encoder;
        }

        public ByteArray encode(IStruct message) {
            C_S_PlayerCreateReq struct = (C_S_PlayerCreateReq) message;
            ByteArray byteArray = ByteArray.createNull(4);
            byte[] nicknameBytes = convertString(byteArray, struct.getNickname());
            byteArray.create();
            putString(byteArray, nicknameBytes);
            byteArray.putShort(struct.getSex());
            byteArray.putShort(struct.getVocation());
            return byteArray;
        }
    }
}